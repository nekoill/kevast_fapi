import requests
from requests.auth import HTTPBasicAuth
from fastapi import FastAPI
# from services.backend.app.app import storage_f, init_app  #, init_storage
# from . import schemas, models
# from database import engine

app = FastAPI()
# print(storage_f)
# init_app()
# models.Base.metadata.create_all()
  
# Making a get request
# TODO:

response = requests.get('https://{request.url} / user, ',
            auth = HTTPBasicAuth('user', 'pass'))
  
# print request object
print(response)


def usage() -> None:
    NOISE_1 = '\n\t\t\t\t\t\tHello USER, I am FAPI\n'
    NOISE_2 = '\nUSAGE:\n\t\tstorage (-k | --key) [KEY]\t\t\t\t Look up a value of a provided key\n'  # noqa: E501
    NOISE_3 = '\t\tstorage (-k | --key) [KEY] (-v | --val) [VAL]\t\t Add a specified key-val pair to the database\n'  # noqa: E501
    print(NOISE_1, NOISE_2, NOISE_3)


def index() -> str:
    print('\n\t\t\t\tWELCOME!\n')
    print('\t\tTO THE KEVASTOPY F-API!\n')
    return 'This is the index page'


if __name__ == '__main__':
    usage()
