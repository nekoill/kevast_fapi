from db import db
from domain.user_mgmnt.queries_interface import IUserQueries
from domain.user_mgmnt.user_schema import UserCreateSchema, UserUpdateSchema


Data: dict[int, any] = {
    1: 'id',
    2: 'second',
    3: 3,
    4: [
        0, 1, 4, 8, 8, 14, 34, 42, 69, 88, 420
    ],
    5: [
        'this', 'is', 'the', 'list', 'of', 'strings'
    ],
    6: True,
    7: None,
    8: {
        1: False,
        2: None
    }
}

MData: dict[str, int] = {
    "item_id": 1,
    "title": 2,
    "quantity": 3,
    "indices": 4,
    "strings": 5,
    "penalties": 6,
    "benefits": 7,
    "offers": 8
}

class UserModel(db.Model):
    __dictname__ = "user base"
    
    user_id = db.Column()