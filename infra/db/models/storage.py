from app.app import storage_f
import json


TEST_DATA = {
    {
        'customer_id': 1,
        'full_name': ('Ivan', 'Govno'),
        'sex': 'male',
        'subed_cats': [
            'toys',
            'child_food',
            'household_items'
        ],
        'benefits': None,
        'personal_discounts': None,
        'social_benefits': {
            'disabled': False,
            'pregnant': False,
            'veteran': False,
            'dangerous_Occupation': False
        }
    },
    {
        'customer_id': 2,
        'full_name': ['Alexander', 'TheSoSo'],
        'sex': 'male',
        'subed_cats': [
            'toys',
            'child_food',
            'household_items'
        ],
        'benefits': None,
        'personal_discounts': None,
        'social_benefits': {
            'disabled': False,
            'pregnant': False,
            'veteran': False,
            'dangerous_Occupation': False
        }
    },
    {
        'customer_id': 3,
        'full_name': ['Ivan', 'Govno'],
        'sex': 'yes',
        'subed_cats': [
            'toys',
            'child_food',
            'household_items'
        ],
        'benefits': None,
        'personal_discounts': None,
        'social_benefits': {
            'disabled': False,
            'pregnant': False,
            'veteran': False,
            'dangerous_Occupation': False
        }
    },
    {
        'customer_id': 4,
        'full_name': ['Ivan', 'Govno'],
        'sex': 'yes',
        'subed_cats': [
            'toys',
            'child_food',
            'household_items'
        ],
        'benefits': None,
        'personal_discounts': None,
        'social_benefits': {
            'disabled': False,
            'pregnant': False,
            'veteran': False,
            'dangerous_Occupation': False
        }
    }
}


def check_dbs_vibe(db) -> dict | int:
    if db:
        return db
    else:
        with open(storage_f) as s:
            user_input: str = input('\n\t\tIt appears that the database file is missing\nDo you want to create one? [Y/n]: ')  # noqa: E501
            if user_input not in ('N', 'n'):
                with open(storage_f, 'x') as s:
                    jlo = json.load(s)
                    json.dump(TEST_DATA, jlo, indent=4)
                    return jlo
            else:
                return 41
