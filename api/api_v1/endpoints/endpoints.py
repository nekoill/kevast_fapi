from fastapi import APIRouter, HTTPException, Depends
from starlette import status

from main import usage
from api.api_v1.utils import get_user_services

ROOT = "/"
GET_BY = "/data/get"
POST_BY = "/data/post"

app = FastAPI()


@app.get(ROOT)
async def greet(url: url) -> str | dict:

    if url != ROOT:
        print(usage())
        i = input('Would you like to search for anything?\n(Y)/n')

        if i in ("N", "n"):
            return 0
        else:
            I = input("What is that you're looking for?")

            if I is not None:
                url = GET_BY
    else:
        return {'data': None, 'response': 'Query is empty ({query})'}


@app.get(GET_BY)
def find_by_id(id: int) -> str:
    app.get(GET_BY + "/{id}")


@app.post("/data/add/{key}={val}")
async def SetD(q: str) -> dict:
    return 0


@app.post("/data/post")