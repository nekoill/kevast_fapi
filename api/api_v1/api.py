from fastapi import FastAPI, APIRouter
from starlette import status
from starlette.testclient import TestClient
# from sqlalchemy import Column, Integer, String
# from database import Base

app = FastAPI()


@app.get("/")
def index() -> str:
    return '\n\tWelcome to KeVaSto.Py, the Key-Value Storage!'


async def get_id(item_id: int) -> dict[int,any]:
    return {'item id': item_id, 'info': data}
    # return storage_f.lookup(item_id)
    # return models.User.query.filter(models.User.name.contains(query))


@app.get("/data/get/{key}")
async def get(key: int) -> dict:
    return {'key': key}


@app.post("/data/post/{val}")
async def add(query) -> dict:
    if query is None:
        return {'query': None}
    else:
        return {'query': _}


@app.put("/data/put/{key}:{val}")
async def update(query) -> dict:
    return {'query': query}


TEST_DICT_1: dict = {
    'id': 1,
    'list_val': [1, 4, 8, 34, 42, 69],
    'tuple_val': ('third', 'value'),
    'dict_val': {'value_5': False, 'value_n': True}
}


if __name__ == '__main__':
    index()