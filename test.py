import json
import os.path

t_list_1 = [1, 4, 8, 8, 14, 88]
t_list_2 = ['one', 'four', 'eight', 'eight', 'fourteen', 'eighty-eight']
storage_file = os.path.abspath(
    (__file__).replace("test.py", "db/storage.data")
)


def zipper(l: list[int], l_: list[any]) -> dict[int, str]:
    t_dict: dict[int, str] = {}
    with open(storage_file) as o:
        for item in range(len(l)):
            t_dict.update({l[item]: l_[item]})
        json.dump()
    return t_dict

"""
def zip_lists(l: list[int], l_: list[str]) -> dict[int, str]:
    for i in range(len(l)):
        t_dict.update({l[i]: l_[i]})
    return t_dict


def dump_zipped(f) -> dict:
    # if storage_file is None:
    with open(storage_file, "x") as o:
        jlo = json.load(o)
        json.dump(jlo, o, indent=4)
        return o


storage_f: str = os.path.abspath((__file__).replace('test.py', '/services/backend/db/storage.data'))  # noqa: E501
print(f'\n\n\t\t\tSTORAGE FILE PATH>>> {storage_f} <<<STORAGE FILE PATH\n\n')

try:
    with open(storage_f) as o:
        jlo = json.load(o)
        print('\n\t\t\t\tLine 32:\n')
        print(jlo)
except FileNotFoundError:
    with open(storage_f, 'x') as o:
        print(zip(t_list_1, t_list-2))
        json.dump(TDict, o, indent=4)
        print('\n\t\t\t\tLine 40:\n')
        print(TDict)


def t_func_1(db) -> dict:
#   print(f'\n\t\t`t_list_1`: {t_list_1}\n\t\t`t_list_2`: {t_list_2}\n\t\t`t_dict`: {t_dict}\n\t\t`db`: {db}\n')  # noqa: E501
    with open(db) as od:
        jlo = json.load(od)
        with open(db, "x") as oj:
            json.dump(jlo, oj, indent=4)
            return oj
"""


if __name__ == '__main__':
    print(zipper(t_list_1, t_list_2))
    # print(t_func_1(storage_file))
    # print(dump_zipped(zip_lists(t_list_1, t_list_2)))
