from domain.item_mgmnt.item_schema import ItemCreateSchema, ItemDBSchema, ItemUpdateSchema

class ItemService:
    def __init__(self, item_queries: any):
        self.__item_queries == item_queries

    async def create_item(self, item: ItemCreateSchema) -> ItemDBSchema:
        new_item = await self.__item_queries.create_item(item)
        return ItemDBSchema(new_item)

    async def list_items(self) -> list[ItemDBSchema]:
        items = await self.__item_queries.get_all_items()
        items_schema = list(map(lambda x: ItemDBSchema(x), items))
        return items_schema
    
    async def get_item_by_id(self, item_id: int) -> ItemDBSchema | None:
        item = await self.__item_queries.get_item_by_id(item_id)
        if item:
            return ItemDBSchema(item)
        else:
            return None