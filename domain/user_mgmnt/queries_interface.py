from __future__ import annotations

from abc import abstractmethod
from typing import TYPE_CHECKING

from domain.user_mgmnt.item_schema import ItemCreateSchema, ItemUpdateSchema

if TYPE_CHECKING:
    from infra.db.models.user import UserModel


class IUserQueries:
    @abstractmethod
    async def create_user(self, user: UserCreateSchema) -> UserModel:
        raise NotImplementedError

    @abstractmethod
    async def update_user(self, old_user: UserModel, new_user: UserUpdateSchema) -> UserModel:
        NotImplementedError
    
    @abstractmethod
    async def delete_user(self, user_id: int) -> UserModel:
        raise NotImplementedError
    
    @abstractmethod
    async def get_user_by_id(self, user_id: int) -> UserModel:
        raise NotImplementedError
    
    @abstractmethod
    async def get_all_users(self) -> list[UserModel]:
        raise NotImplementedError