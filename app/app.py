import json
import os.path
# from fastapi import FastAPI

storage_f = os.path.abspath(
    (__file__).replace("/app/app.py", "/db/storage.data"))
print(storage_f)

# Both `T_List_`s are of len(Tlistn) == 10
# The idea is to basically zip them together like with zipWith` in Haskell
# Not to get fucking tuples as output
# The fuck am I supposed to do with those?!
T_List_1: list[int] = [0, 1, 4, 8, 14, 18, 34, 42, 69, 420]
T_List_2: list[str] = [
    "one",
    "two",
    "three",
    "yes",
    "I",
    "gave",
    "up",
    "immediately",
    "eight",
    "woohoo!"
]  # noqa: E501
T_Dict: dict[int, str] = {}


def dict_update(db: dict[int, str]) -> dict[int, str]:
    for index, val in zip(range(len(T_List_1)), T_List_2):
        db.update({index: val})
        print(db)
        return db

dict_update(T_Dict)


def chck_storage(db) -> dict:  # noqa
    with open(db) as ofx:
        jlo_ = json.load(ofx)
        for digit in T_List_1:
            for word in T_List_2:
                T_Dict.update({digit: word})
                json.dump(T_Dict, ofx, indent=4)
            return jlo_
        return db


if __name__ == "__main__":
    print(chck_storage(storage_f))
